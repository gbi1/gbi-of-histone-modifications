# Graph–based integration of histone modifications profiles #

This repository contains data and code of the paper "Graph-based integration of histone modification profiles" 

## Overview

Histone modifications are epigenetic marks which modify the structure of chromatin, with the effect of regulating gene expression. It is known that different types of histone modifications can interact with the effect of inhibiting or promoting gene transcription. This, in turn, impacts on the process of phenotype differentiation. 

The data and code available in this repository allow to process signals from six histone modification marks referred to a set of 24 cell types involved in haematopoiesis, and to integrate them in a graph-based setup. In particular, the feature extracted from each histone modification signal consist in the count of peaks per gene in the signal of each cell sample. Then, after the cell samples profiles are properly normalized, they are organized into six similarity networks (each one computed based on a single histone modification) and then integrated into a unique graph. Finally, the sensitiveness of the model is verified by the definition of a score to evaluate a hypothesis on the biological process of haematopoiesis. 

## Prerequisites

The code for the feature extraction procedure is implemented using C.
The integration procedure is implemented in R. For the R code, be sure to have the following packages installed:

- igraph
- rlist
- sets

## Histone modification data download ##

The files of the histone modification profiles can be downloaded from the IHEC Data Portal at
    
    https://epigenomesportal.ca/ihec/

They are in compressed *.bigWig* format. They can be decompressed and converted to *.Bed* files by using the *bigWigToBedGraph* tool available at:
		
	http://hgdownload.cse.ucsc.edu/admin/exe/linux.x86_64/

## Code for feature extraction ##

- the script **norm-binner.c** is used for discretizing the genome-wide signal of a histone modification. The size of the window for the normalization is set to 20 but can be customized.
The input is a *.Bed* file of four columns registering the location and the intensity of the signal. The output is a *.Bed* file of four columns where: 
    - the first column is the chromosome number;
    - the second and third column indicate the beginning and the end of the chromosomic region of  interest; 
    - the fourth column is the averaged signal over that interval.

- the script **ePeak2.c** is the peak calling algorithm. The input is a *.Bed* file (obtained, for example, by the **norm-binner.c** code). The output is a *.Bed* file where:
    - first column is the chromosome number;
	- second and third columns are the beginning and the end of the chromosomic region of interest;
	- third column is the signal intensity over the peak;
	- fourth column is the Z-score of the detected peak;
  
 
- the files obtained with **ePeak2.c** are used for extracting features, i.e. for counting the number of peaks for each gene. The list of genes and their location in the genome can be downloaded at:

        http://ftp.ensembl.org/pub/release-76/gtf/homo_sapiens/


**NOTE:** in our experiments, peaks with Z–score ranging under a threshold value of 2 were filtered out.

## Available pre-processed data ##

- The *.zip* files **counts_1** and **counts_2** contain the *.txt* files obtained after counting peaks per gene for each histone modification and for each sample:

	- H3K27ac_counts.txt
	- H3K27me3_counts.txt 
	- H3K36me3_counts.txt 
	- H3K4me1_counts.txt 
	- H3K4me3_counts.txt 
	- H3K9me3_counts.txt.

The first column of each file contains the cell type, each of the next columns corresponds a gene.
They also contain 6 files named "sample_id_H3K****.txt", each one consisting in a single column with the sample_id of each cell sample (row) in the count matrices.

- The folder **metadata** contains metadata of the samples of each single modification, in *.csv* format.

- For the RPKM normalization, the length of genes (in bp) was again obtained from: 

	    http://ftp.ensembl.org/pub/release-76/gtf/homo_sapiens/

by subtracting the entry "start" from the entry "end". The file *db_gene_lengths.txt*
contains the lengths of the considered genes. The first column contains the gene id, the second column is the length of the corresponding gene (in bp).

- The file "label_hypothesis.txt" contains the labels of each cell type according to the myeloid-lymphoid differentiation. The first column contains "M" or "L" according to "Myeloid" or "Lymphoid" for the cell type in the second column.
Cell types are in alphabetical order.

## Instructions for the R implementation of the hypothesis testing outline ##

- For data normalization (CPM and RPKM) performed in the paper, use the **R** functions  **cpm** and **rpkm** from the package **edgeR** (see link below for documentation):
  
        https://www.rdocumentation.org/packages/edgeR/versions/3.14.0/topics/cpm 

- To compute the average profile of samples of the same type use the **R** function **aggregate** from the package **stats** (https://www.rdocumentation.org/packages/stats/versions/3.6.2/topics/aggregate) on the normalized matrices, with the following parameters:
	
	`aggregate(matrix, by=list(matrix$cell_type),fun=MEAN)`
 
Notice that at the end of this step cell types are ordered alphabetically.

- For cleaning the matrices of samples from flat genes do the following:

    - Use the "kmeans" function as in the following command line:
		
	`k = kmeans(matrix.of.celltypes,50,iter.max=300,algorithm='Lloyd')`
 		
	- Exclude genes with a maximum value over a centroid which is <= to the lowest 10% of the expression interval.

- To compute distance matrices between cell types use the R function **distance** as follows:

	`d = distance(matrix,method='SQeuclidean')`

- Distance matrices are the input for the SNF code, available at this link: 

	    http://compbio.cs.toronto.edu/SNF/SNF/Software.html

- To turn the fused matrix into a dissimilarity matrix, use the code available in the R script **reverse_fusion.R**. 

- The script **hyp_test.R** is the code for computing the graph-cut based score for hypothesis testing. Before running **hyp_test.R**, you have to compile the **greedyCut** function in the available script **greedy_cut_function.R** which, in turn, requires compiling the **attraction_vertex.R** function.

## Citation ##
 
If this code was useful to you, please consider citing the reference paper:

Baccini Federica, Monica Bianchini, and Filippo Geraci. "Graph-Based Integration of Histone Modification Profiles." Mathematics 10.11 (2022): 1842.
https://www.mdpi.com/2227-7390/10/11/1842




