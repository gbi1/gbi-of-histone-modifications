/* Algorithm that discretize a histone modification signal by averaging it within a window of user-defined width */

/* INPUT: .bed file where
	-first column is the chromosome number;
	-second and third column are integers indicating, respectively, contiguous bp in the chromosome;
	-fourth column is the signal intensity over the interval;
   OUTPUT: .bed file where 
	-first column: "chr" is the chromosome number 
	-second column: "init_pos" is the initial position in the chromosome
	-third column: "init_pos + length" is the final position in the interval
	-fourth column: "signal" is the signal intensity normalized over the interval
*/


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <assert.h>

#define MAX_LABEL_LENGTH  256  /*  Max size of a label of a sequence  */
#define WINDOW_SIZE 20   /* Window size for discretization is set to 20, change window size to customize experiments */

struct bedline {
  char chr[MAX_LABEL_LENGTH];
  long int init_pos;
  int length;
  float signal;
  _Bool to_merge;
};

FILE *read_next (FILE *pf, struct bedline *elem, _Bool isFemale) {
  long int end_pos;
  if (pf == NULL) {
    strcpy (elem->chr, "eof");
    elem->init_pos = 0;
    elem->length = 0;
    elem->signal = 0;
    return NULL;
  }
  if (!feof (pf)) {
    fscanf (pf, "%s %ld %ld %f\n",elem->chr, &elem->init_pos, &end_pos, &elem->signal);
    elem->length = end_pos - elem->init_pos;
    /*  Set signal to 0 on chrY of females  */
    if (isFemale) {
      if (strcmp (elem->chr, "chrY") == 0) elem->signal = 0;
    }
  }
  else {
    strcpy (elem->chr, "NONE");
    elem->init_pos = 0;
    elem->length = 0;
    elem->signal = 0;
    elem->to_merge = false;
    fclose (pf);
    return NULL;
  }
  return pf;
}

_Bool all_done (struct bedline *elems) {
  if (strcmp (elems[0].chr, "eof") == 0) return true;
  return false;
}

/*  str is a vector of max three elements */
int split_interval (struct bedline **str, struct bedline *elem) {
  int remaining = elem->length;
  int window_size;
  strcpy (str[0]->chr, elem->chr);
  str[0]->signal = elem->signal;
  str[0]->init_pos = elem->init_pos - (elem->init_pos % WINDOW_SIZE);
  window_size = WINDOW_SIZE - (elem->init_pos % WINDOW_SIZE);
  if (remaining <= window_size) {
    str[0]->length = remaining;
    return 1;
  }
  str[0]->length = window_size;
  remaining -= window_size;

  str[1]->init_pos = str[0]->init_pos + WINDOW_SIZE;
  strcpy (str[1]->chr, elem->chr);
  str[1]->signal = elem->signal;
  if (remaining <= WINDOW_SIZE) {
    str[1]->length = remaining;
    return 2;
  }
  str[1]->length = (remaining / WINDOW_SIZE) * WINDOW_SIZE;

  strcpy (str[2]->chr, elem->chr);
  str[2]->signal = elem->signal;
  str[2]->init_pos = str[1]->init_pos + str[1]->length;
  str[2]->length = remaining - str[1]->length;
  return 3;
}

void __copy_interval (struct bedline *i1, struct bedline *i2) {
  strcpy (i2->chr, i1->chr);
  i2->init_pos = i1->init_pos;
  i2->length = i1->length;
  i2->signal = i1->signal;
  i2->to_merge = i1->to_merge;
}


void push_back (struct bedline *this, struct bedline *prev, FILE *out) {
  if (strcmp (prev->chr, "NONE") == 0) {
    __copy_interval (this, prev);
    return;
  }
  if (strcmp (prev->chr, this->chr) == 0 && \
      prev->init_pos + prev->length == this->init_pos && \
      prev->signal == this->signal) {
    prev->length += this->length;
    return;
  }
  fprintf (out, "%s %ld %ld %d\n", prev->chr, prev->init_pos, prev->init_pos + prev->length, (int) (prev->signal * 10));
  __copy_interval (this, prev);
}


int main (int argc, char *argv[]) {
  FILE *pf, *out;
  struct bedline *elem;
  struct bedline *str[3];
  struct bedline interval, prev_interval;
  _Bool female;
  int i, tot_str;
  if (argc != 3 && argc != 4) {
    printf ("%s infile outfile [M/F]\n", argv[0]);
    exit (-1);
  }
  pf = fopen (argv[1], "r");
  out = fopen (argv[2], "w");
  if (pf == NULL || out == NULL) {
    printf ("bad news - unable to open file\n");
    exit (-1);
  }
  female = false;  /*  If female set signal on Y equals to 0 regardless the value  */
  if (argc == 4) {
    if (argv[3][0] == 'f'|| argv[3][0] == 'F') female = true;
  }
  str[0] = (struct bedline *) malloc (sizeof (struct bedline));
  str[1] = (struct bedline *) malloc (sizeof (struct bedline));
  str[2] = (struct bedline *) malloc (sizeof (struct bedline));
  elem = (struct bedline *) malloc (sizeof (struct bedline));
  if (elem == NULL || str[0] == NULL || str[1] == NULL || str[2] == NULL) {
    printf ("bad news - memory failure\n");
    exit (-1);
  }

  /*  Initialization */
  pf = read_next (pf, elem, female);
  strcpy (interval.chr, "NONE");
  interval.to_merge = false;
  interval.signal = 0;
  strcpy (prev_interval.chr, "NONE");
  prev_interval.to_merge = false;
  prev_interval.signal = 0;
  tot_str = split_interval (str, elem);

  while (all_done (elem) == false) {
    /*printf ("\nELEM %s %d %d %f\n", elem->chr, elem->init_pos, elem->length, elem->signal);*/
    tot_str = split_interval (str, elem);
    for (i = 0; i < tot_str; i ++) {
      /*printf ("E%s %ld %d %f\n", str[i]->chr, str[i]->init_pos, str[i]->length, str[i]->signal); */
      /*  The elem is part of the bucket  */
      if (strcmp (str[i]->chr, interval.chr) == 0 && str[i]->init_pos == interval.init_pos) {
        interval.to_merge = true;
        interval.signal += (str[i]->signal * str[i]->length);
        interval.length += str[i]->length;   /*  just debug  */
      }
      else {  /*  New interval  */
        if (interval.to_merge == true) {
          interval.signal /= WINDOW_SIZE;
          assert (interval.length <= WINDOW_SIZE);
          interval.length = WINDOW_SIZE;
        }
        /*  Merge interval / interval_prev or print interval_prev  */
        push_back (&interval, &prev_interval, out);
        /*  Create the new interval  */
        strcpy (interval.chr, str[i]->chr);
        interval.init_pos = str[i]->init_pos;
        interval.length = str[i]->length;
        if (interval.length < WINDOW_SIZE) {  /*  Interval smaller than the window  */
          interval.to_merge = true;
          interval.signal = str[i]->signal * str[i]->length;
        }
        else {
          interval.signal = str[i]->signal;
          interval.to_merge = false;
        }
      }
    }
    pf = read_next (pf, elem, female);
  }
  fprintf (out, "%s %ld %ld %d\n", prev_interval.chr, prev_interval.init_pos, prev_interval.init_pos + prev_interval.length, (int) (prev_interval.signal * 10));
  fclose (out);
  return 0;
}
