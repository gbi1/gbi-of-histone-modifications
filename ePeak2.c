/* ePeak2 algorithm for extracting peaks from discretized genome-wide histone modification signals */

/*INPUT: a .bed file where:
	-first column is the chromosome number;
	-second and third column are integers indicating, respectively, the beginning and the end of a chromosome location;
	-fourth column is the signal intensity over the interval;
  OUTPUT: a .bed file where: 
	-first column: "label" is the label of the chromosome where the peak has been detected
	-second column: "init_pos" is the integer indicating the beginning of the chromosome location where there is a peak
	-third column: "end_pos" is the integer indicating the end of the chromosome location where there is a peak
	-fourth column: "signal" is the value of the signal intensity over the peak
	-fifth column: "peak_strength" is the Z-score of the peak
*/



#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <ctype.h>

#define MAX_LABEL_LENGTH  256  /*  Max size of a label of a sequence  */

struct coord_t {
  char label[MAX_LABEL_LENGTH];
  long init_pos;
  long end_pos;
  char strand;
  float signal;
  double peak_strength;
  _Bool isPeak;
  struct coord_t *prev;
  struct coord_t *next;
};

struct coord_t *read_parse_bed (char *filename);
_Bool split_coords (char *coord_str, struct coord_t *coord);
double __ePeak_avrg (struct coord_t **interval, int interval_pos);
double __ePeak_sigma (struct coord_t **interval, int interval_pos);
void __ePeak_set_peaks (struct coord_t **interval, int interval_pos);
void __ePeak_extend_peaks (struct coord_t **interval, int interval_pos);
void dump (struct coord_t **interval, int interval_pos);
void merge_peaks (struct coord_t *bed_list, _Bool use_int);
void save_intervals (char *filename, struct coord_t *bed_list, _Bool onlyPeaks);

struct coord_t *read_parse_bed (char *filename) {
  FILE *pf;
  struct coord_t *this_line, *head, *next_line;
  float fuffa;
  pf = fopen (filename, "r");
  if (pf == NULL) {
    printf ("bad news (files not open)\n");
    return NULL;
  }
  head = (struct coord_t *) malloc (sizeof (struct coord_t));
  if (head == NULL) {
    printf ("bad news (memory allocation)\n");
    return NULL;
  }
  head->prev = NULL;
  head->next = NULL;
  fscanf (pf, "%s %ld %ld %f %f\n",head->label, &head->init_pos, &head->end_pos, &head->signal, &fuffa);
  head->isPeak = false;
  head->peak_strength = 0;
  this_line = head;
  next_line = head;
  while (!feof (pf)) {
    /*  Differ in case two intervals have just been merged  */
    if (next_line == this_line) {
      next_line = (struct coord_t *) malloc (sizeof (struct coord_t));
      if (next_line == NULL) {
        printf ("bad news (memory allocation)\n");
        return NULL;
      }
      next_line->prev = this_line;
      next_line->next = NULL;
      this_line->next = next_line;
    }
    fscanf (pf, "%s %ld %ld %f %f\n",next_line->label, &next_line->init_pos, &next_line->end_pos, &next_line->signal, &fuffa);
    next_line->isPeak = false;
    next_line->peak_strength = 0;
    if (this_line->signal == next_line->signal && \
        this_line->end_pos == next_line->init_pos && \
        strcmp (this_line->label, next_line->label) == 0) {
      this_line->end_pos = next_line->end_pos;
    }
    else {
      this_line = this_line->next;
    }
  }
  fclose (pf);
  return head;
}


_Bool split_coords (char *coord_str, struct coord_t *coord) {
  int i, j;
  char buff[32];
  long int switcher;
  _Bool proc_flow = false;  /*  Used to ensure all the components are extracted  */
  /*   Extract the sequence name  */
  for (i = 0; coord_str[i] != '\0'; i ++) {
    proc_flow = true;   /*  Pass thorugh this loop  */
    if (coord_str[i] != ':')
      coord->label[i] = coord_str[i];
    else {
      if (i == 0) return false;   /*  No sequence information  */
      coord->label[i] = '\0';
      i ++;  /*  skip :  */
      break;
    }
  }
  /*  Check it passed through the loop  */
  if (proc_flow == false) return false;
  else proc_flow = true;
  /*   Extract the sequence init_coord  */
  for (j = 0; coord_str[i] != '\0'; i ++, j ++) {
    proc_flow = true;   /*  Pass thorugh this loop  */
    if (coord_str[i] != '-') {
      if (isdigit (coord_str[i]) == 0) return false;  /*  There should be a digit  */
      buff[j] = coord_str[i];
    }
    else {
      if (j == 0) return false;  /*  First coordinate is empty  */
      buff[j] = '\0';
      coord->init_pos = atol (buff);
      i ++;  /*  skip -  */
      break;
    }
  }
  /*  Check it passed through the loop  */
  if (proc_flow == false) return false;
  else proc_flow = true;
  /*   Extract the sequence end_coord  */
  for (j = 0; coord_str[i] != '\0'; i ++, j ++) {
    proc_flow = true;   /*  Pass thorugh this loop  */
    if (isdigit (coord_str[i]) == 0) break;
    buff[j] = coord_str[i];
  }
  if (j == 0) return false;  /*  Final coordinate missing  */
  buff[j] = '\0';
  coord->end_pos = atol (buff);
  /*  Check it passed through the loop  */
  if (proc_flow == false) return false;
  else proc_flow = true;

  /*  Extract the strand  */
  if (coord_str[i] == '+' || coord_str[i] == '-') coord->strand = coord_str[i];
  else coord->strand = '\0';  /*  No strand information  */
  /*  Inverted coordinates to indicate stand -  */
  if (coord->end_pos < coord->init_pos) {
    /*  Incoerent state stand + but invert coordinates means stand -  */
    if (coord->strand == '+') return false;
    switcher = coord->end_pos;
    coord->end_pos = coord->init_pos;
    coord->init_pos = switcher;
    coord->strand = '-';
  }
  return true;
}

double __ePeak_avrg (struct coord_t **interval, int interval_pos) {
  double avrg = 0;
  int i;
  for (i = 0; i < interval_pos; i ++) {
    avrg += interval[i]->signal;
  }
  avrg /= interval_pos;
  return avrg;
}

double __ePeak_sigma (struct coord_t **interval, int interval_pos) {
  int i;
  double sigma = 0;
  double avrg = __ePeak_avrg (interval, interval_pos);
  for (i = 0; i < interval_pos; i ++) {
    sigma += ((interval[i]->signal - avrg) * (interval[i]->signal - avrg));
  }
  sigma /= interval_pos;
  return sqrt (sigma);
}

void __ePeak_extend_peaks (struct coord_t **interval, int interval_pos) {
  int i, last_peak;
  double sigma, avrg;

  sigma = __ePeak_sigma (interval, interval_pos);
  avrg = __ePeak_avrg (interval, interval_pos);

  /* not applicable to very small intervals  */
  if (interval_pos < 3) return;
    /*  Ignore negative peaks  */
  for (i = 0; i < interval_pos; i ++) {
    if (interval[i]->peak_strength < 0) interval[i]->isPeak = false;
  }
  /*  Extend a peak forward - XXX set peak interval[i+1] */
  last_peak = 0;
  for (i = 0; i < interval_pos - 1; i ++) {
    /* if it is not a peak, or next is already a peak */
    if (interval[i]->isPeak == false || interval[i + 1]->isPeak == true) {
      last_peak = i + 1;
      continue;
    }
    /*  A peak cannot extend to a value higher than the peak  */
    if (interval[i + 1]->signal > interval[last_peak]->signal) continue;
    /*  To be extended must be contiguous  */
    if (interval[i + 1]->init_pos != interval[i]->end_pos) continue;
    /*  Does not extend the peak if the signal strangh is blowing down  */
    if (interval[i + 1]->peak_strength < interval[last_peak]->peak_strength - 1)
      continue;
    if (interval[i + 1]->peak_strength < 0) continue;
    if (interval[i+1]->signal > interval[last_peak]->signal - sigma) {
      interval[i+1]->isPeak = true;
    }
  }

  /*  Extend a peak backword - XXX set peak interval[i-1] */
  last_peak = -1;
  for (i = interval_pos - 1; i > 0; i --) {
    /* if it is not a peak, or next is already a peak */
    if (interval[i]->isPeak == true && interval[i - 1]->isPeak == false) {
      if (last_peak == -1) last_peak = i;

      /*  A peak cannot extend to a value higher than the peak  */
      if (interval[i - 1]->signal > interval[last_peak]->signal) {
        last_peak = -1;  /*  Unset  */
        continue;
      }
      /*  To be extended must be contiguous  */
      if (interval[i - 1]->end_pos != interval[i]->init_pos) {
        last_peak = -1;  /*  Unset  */
        continue;
      }
      /*  Does not extend the peak if the signal strangh is blowing down  */
      if (interval[i - 1]->peak_strength < interval[last_peak]->peak_strength - 1) {
        last_peak = -1;  /*  Unset  */
        continue;
      }
      if (interval[i - 1]->peak_strength < 0) {
        last_peak = -1;  /*  Unset  */
        continue;
      }
      if (interval[i-1]->signal > interval[last_peak]->signal - sigma)
        interval[i-1]->isPeak = true;
    }
    else {
      last_peak = -1;  /*  Unset  */
    }
  }

}

void __ePeak_set_peaks (struct coord_t **interval, int interval_pos) {
  int this_elem;
  double sigma, avrg;

  sigma = __ePeak_sigma (interval, interval_pos);
  avrg = __ePeak_avrg (interval, interval_pos);

  /* not applicable to very small intervals  */
  if (interval_pos < 3) return;
  /*  Find peak at the beginning of the stream  */
  if (interval[0]->signal > interval[1]->signal) {
    interval[0]->isPeak = true;
  }
  interval[0]->peak_strength = (interval[0]->signal - avrg) / sigma;

  /*  Find peak in the stream of data  */
  for (this_elem = 1; this_elem < interval_pos - 1; this_elem ++) {
    if ((interval[this_elem - 1]->signal < interval[this_elem]->signal) &&
        (interval[this_elem + 1]->signal < interval[this_elem]->signal)) {
      interval[this_elem]->isPeak = true;
    }
    interval[this_elem]->peak_strength = (interval[this_elem]->signal - avrg) / sigma;
  }

  /*  Find peak at the end of the stream  */
  if (interval[interval_pos - 1]->signal > interval[interval_pos - 2]->signal)  {
    interval[interval_pos - 1]->isPeak = true;
  }
  interval[interval_pos - 1]->peak_strength = (interval[interval_pos - 1]->signal - avrg) / sigma;
}

void dump (struct coord_t **interval, int interval_pos) {
  int i;
  double sigma, avrg;
  sigma = __ePeak_sigma (interval, interval_pos);
  avrg = __ePeak_avrg (interval, interval_pos);
  printf ("INTERVAL %d %f %f\n", interval_pos, avrg, sigma);
  for (i = 0; i < interval_pos; i ++) {
    printf ("%s %ld %ld %f %f ", interval[i]->label, interval[i]->init_pos, interval[i]->end_pos, interval[i]->signal, interval[i]->peak_strength);
    if (interval[i]->isPeak == false) printf ("\n");
    if (interval[i]->isPeak == true) printf (" *\n");
  }
  fflush (stdout);
}

/*  Calling this produces a modified signal in bed files  */
void merge_peaks (struct coord_t *bed_list, _Bool use_int) {
  int interval_length;
  double tot_signal, tot_strength;
  struct coord_t *elem, *eop, *tmp, *tmp2;
  for (elem = bed_list; elem->next != NULL; elem = elem->next) {
    /*  If is the last of the list no need to join  */
    if (elem->isPeak == true && elem->next->isPeak == true) {
      tot_signal = elem->signal * (elem->end_pos - elem->init_pos);
      tot_strength = elem->peak_strength * (elem->end_pos - elem->init_pos);
      interval_length = elem->end_pos - elem->init_pos;
      for (eop = elem->next; \
           eop->isPeak == true && eop->init_pos == eop->prev->end_pos; \
           eop = eop->next) {
        tot_signal += (eop->signal * (eop->end_pos - eop->init_pos));
        tot_strength += (eop->peak_strength * (eop->end_pos - eop->init_pos));
        interval_length += (eop->end_pos - eop->init_pos);
        /*  The sequence finish during a peak  */
        if (eop->next == NULL) {
          eop = NULL;
          break;
        }
      }
      /*  The case of two contiguous peaks but not contiguous coordinates  */
      if (eop == elem->next) continue;
      /*  Modify the elem with the new values  */
      if (use_int == true)
        elem->signal = (int) (tot_signal / interval_length);
      else
        elem->signal = tot_signal / interval_length;
      elem->peak_strength = tot_strength / interval_length;
      elem->end_pos = elem->init_pos + interval_length;
      /*  Free memory and merge lists  */
      tmp = elem->next;
      while (tmp != eop) {
        tmp2 = tmp->next;
        free (tmp);
        tmp = tmp2;
      }
      elem->next = eop;
      /*  Update eop if not at the end of the list  */
      if (eop != NULL) eop->prev = elem;
      /*  Happens only merging intervals at the very end of the list  */
      if (elem->next == NULL) break;
    } /*  if (elem->isPeak == true && elem->next->isPeak == true)  */
  }   /*  for (elem = bed_list; elem->next != NULL; elem = elem->next)   */
}

void save_intervals (char *filename, struct coord_t *bed_list, _Bool onlyPeaks) {
  int printed_signal;
  struct coord_t *elem;
  FILE *out_bed;
  out_bed = fopen (filename, "w");
  if (out_bed == NULL) out_bed = stdout;
  for (elem = bed_list; elem != NULL; elem = elem->next) {
    if (onlyPeaks == true && elem->isPeak == false) continue;
    /*  Assign strength 0 to elements that are not peak*/
    if (elem->isPeak == false) elem->peak_strength = 0;
    /*  Does not report lines without signal  */
    if (elem->signal == 0 &&  elem->peak_strength == 0) continue;
    printed_signal = (int) elem->signal;
    if (printed_signal == elem->signal)  { /* is integer ??  */
      if (elem->peak_strength == 0)
        fprintf (out_bed, "%s %ld %ld %d 0\n", elem->label, elem->init_pos, elem->end_pos, printed_signal);
      else
        fprintf (out_bed, "%s %ld %ld %d %.3f\n", elem->label, elem->init_pos, elem->end_pos, printed_signal, elem->peak_strength);
    }
    else {
      if (elem->peak_strength == 0)
      fprintf (out_bed, "%s %ld %ld %.3f 0\n", elem->label, elem->init_pos, elem->end_pos, elem->signal);
      else
      fprintf (out_bed, "%s %ld %ld %.3f %.3f\n", elem->label, elem->init_pos, elem->end_pos, elem->signal, elem->peak_strength); /*print signal and zscore of a peak*/
    }
  }
  if (out_bed != stdout) fclose (out_bed);
}

int main (int argc, char *argv[]) {
  long last_coord = -1;
  struct coord_t *bed_file, *bed_line;
  struct coord_t *coords = NULL;
  struct coord_t **interval = NULL;
  int interval_length = 0;
  int interval_pos = 0;
  bed_file = read_parse_bed (argv[1]);
  if (bed_file == NULL) {
    printf ("Some file refused to open\n");
    exit (-1);
  }
  if (argc == 5) {
    coords = (struct coord_t *) malloc (sizeof (struct coord_t));
    if (coords == NULL) exit (-1);
    if (split_coords (argv[4], coords) == false) {
      free (coords);
      coords = NULL;
    }
  }

  for (bed_line = bed_file; bed_line != NULL; bed_line = bed_line->next) {
    /*  This is only when restrict to a subset of the genome  */
    if (coords != NULL) {
      if (strcmp (coords->label, bed_line->label) != 0) continue;
      if (bed_line->init_pos < coords->init_pos) continue;
      /* analyze last interval and terminates */
      if (bed_line->end_pos > coords->end_pos) break;
    }
    /*  Starts to analyze a new interval  */
    if (bed_line->init_pos != last_coord) {

      /*  Dump statistics  */
      if (last_coord != -1) {
        __ePeak_set_peaks (interval, interval_pos);
        __ePeak_extend_peaks (interval, interval_pos);
          dump (interval, interval_pos);
        interval_pos = 0;
      }
    }

    /*  Consider realloc of intervals  */
    if (interval_pos == interval_length) {
      interval = (struct coord_t **) realloc (interval, (interval_length + 1000) * sizeof (struct coord_t *));
      if (interval == NULL) exit (-1);  /* Memory failure  */
      for (; interval_length < interval_pos + 1000; interval_length ++)
        interval[interval_length] = NULL;
    }
    interval[interval_pos] = bed_line;
    interval_pos ++;
    last_coord = bed_line->end_pos;
  }
  __ePeak_set_peaks (interval, interval_pos);
  __ePeak_extend_peaks (interval, interval_pos);
  dump (interval, interval_pos);
  if (coords != NULL) free (coords);
  merge_peaks (bed_file, true);
  save_intervals (argv[2], bed_file, false);
  return 0;
}
